<?php

use Tests\TestCase;

class ConversionsControllerTest extends TestCase
{
    public function testGetViewTemperature()
    {
        $temperature = 0;
        $uniteInitial = "°C";
        $unite = "°F";
        $response = $this->get('/temperature', ['temperatureHTML' => $temperature, 'uniteInitiale' => $uniteInitial,
            'unite' => $unite, 'resultHTML' => '']);

        $content = $response->content();

        $this->assertTrue(str_contains($content, '<h2 id="title-page">Température</h2>'));
    }

    public function testPostCalcTemperature(){
        $temperature = 5;
        $uniteInitial = "°F";
        $unite = "°C";

        $response = $this->call('POST','/temperature',
            ['temperatureVal' => $temperature, 'uniteInitiale' => $uniteInitial,
                'unite' => $unite, 'resultHTML' => '']);
        $expected = '5 °F équivaut à -15 °C.';
        $result = $response->viewData('resultHTML');

        $this->assertSame($expected,$result);
        $this->assertSame($temperature,$response->viewData('temperatureHTML'));
        $this->assertSame($uniteInitial,$response->viewData('uniteInitiale'));
        $this->assertSame($unite,$response->viewData('unite'));
    }

}


