<?php

use App\Http\Controllers\Calculs\Conversions;
use Tests\TestCase;

class ConversionsTest extends TestCase
{
    public function testConvertTempCToF(){
        $temperature = 5;
        $uniteInitial = "°C";
        $unite = "°F";

        $resultat = Conversions::convertTemp($temperature, $uniteInitial, $unite);
        $expected = 41;

        $this->assertSame($expected, $resultat);
    }

    public function testConvertTempFToC(){
        $temperature = 41;
        $unite = "°C";
        $uniteInitial = "°F";

        $resultat = Conversions::convertTemp($temperature, $uniteInitial, $unite);
        $expected = 5;

        $this->assertSame($expected, $resultat);
    }
    public function testConvertTempFToF(){
        $temperature = 41;
        $unite = "°F";
        $uniteInitial = "°F";

        $resultat = Conversions::convertTemp($temperature, $uniteInitial, $unite);
        $expected = 41;

        $this->assertSame($expected, $resultat);
    }
    public function testConvertTempCToC(){
        $temperature = 5;
        $unite = "°C";
        $uniteInitial = "°C";

        $resultat = Conversions::convertTemp($temperature, $uniteInitial, $unite);
        $expected = 5;

        $this->assertSame($expected, $resultat);
    }
}
