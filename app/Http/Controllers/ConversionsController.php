<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Calculs\Conversions;

class ConversionsController extends Controller
{
    public function getViewTemperature()
    {
		return view('temperature', [
            'temperatureHTML' => 0,
            'uniteInitiale' => "",
            'unite' => "",
            'resultHTML' => 0,
        ]);
    }

    public function calcTemperature(){
        $temperature = request('temperatureVal');
        $uniteInitial = request('uniteInitiale');
        $unite = request('unite');
        $res = Conversions::convertTemp($temperature,$uniteInitial,$unite);
        $result = $temperature." ".$uniteInitial. " "."équivaut à ".$res. " ".$unite.".";
       return view('temperature', [
           'temperatureHTML' => $temperature,
           'uniteInitiale' => $uniteInitial,
           'unite' => $unite,
           'resultHTML' => $result,
       ]);
    }
}
