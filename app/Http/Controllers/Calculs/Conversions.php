<?php

namespace App\Http\Controllers\Calculs;

class Conversions
{
    public static function convertTemp($temperature, $uniteInitiale, $unite){
        if($uniteInitiale === "°C" && $unite === "°F"){
           $result = ($temperature*9/5) +32;
        }
        elseif ($unite === "°C" && $uniteInitiale === "°F"){
            $result = ($temperature-32)*5/9;
        }
        else{
            $result = $temperature;
        }
        return $result;
    }
}
